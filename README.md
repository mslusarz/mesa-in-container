# Podman (Docker) images for Mesa

Build an image:

```
./image-rebuild-ubuntu-22.04.sh
```
or
```
./image-rebuild-fedora-36.sh
```

Start a container:

```
./run.sh my-ubu ubuntu-22.04
```
or
```
./run.sh my-fed fedora-36
```

Inside a container:
```
cd mesa
git fetch
git checkout -f origin/main
./build.sh

glxinfo -B
glxgears

apt install steam
steam
```

Ctrl-D exits Podman

Outside container:

Reattach to an existing container:
```
podman container start my-ubu
podman container attach my-ubu
```
or
```
podman container start my-fed
podman container attach my-fed
```


Drop a container:
```
podman container rm my-ubu
```
or
```
podman container rm my-fed
```

Note:
If environment variable LOCAL is set to 1 during build.sh, then mesa will be
installed to a local directory. Every command that is supposed to use it must
be prepended with `mygl.sh`. In Steam this requires: RMB on a game,
Properties -> General -> Set launch options: /root/bin/mygl.sh %command%
