#!/bin/bash -ex

if [ "$LOCAL" = "1" ]; then
	prefix=~/mesa-bin
else
	prefix=/usr
fi

meson .. --prefix $prefix --libdir $prefix/lib/i386-linux-gnu/ --cross-file=../cross_file.txt -Dgallium-drivers=iris,crocus -Dvulkan-drivers=intel -Dllvm=false "$@"
